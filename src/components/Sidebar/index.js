import React, {useState} from 'react'
import {CloseIcon, Icon, SidebarContainer,  SidebarWrapper, SidebarLink, SidebarMenu, SidebarBg} from './SidebarElements'
import SideBg from '../../images/car.svg'
import { Modal } from '../Form/Modal/modal'

const Sidebar = ({isOpen, toggle}) => {
    const [showModal, setShowModal] = useState(false)

    const openModal = ()=>{
        setShowModal(prev => !prev)
      }
    

    return (
        <>
        <SidebarContainer isOpen={isOpen} >
            <Icon onClick={toggle}>
                <CloseIcon />
            </Icon>
            <SidebarWrapper>
                <SidebarMenu>
            <SidebarBg src={SideBg} />

                <SidebarLink  to='/' onClick={toggle}>
                        Home
                    </SidebarLink>
                    <SidebarLink  to='/concessionaire' onClick={toggle}>
                        Concessionaire
                    </SidebarLink>
                    <SidebarLink to='/ecology' onClick={toggle}>
                        Ecology
                    </SidebarLink> 
                    <SidebarLink to='/contact-us' onClick={toggle}>
                        Contact Us
                    </SidebarLink>
                    <SidebarLink to='/customer-service' onClick={toggle}>
                        Customer Service
                    </SidebarLink>
                    <SidebarLink onClick={toggle}  onClick={openModal}>
                        Register in
                    </SidebarLink>
                </SidebarMenu>
                
            </SidebarWrapper>
        </SidebarContainer>

        <Modal showModal={showModal} setShowModal={setShowModal}/>
        </>
    )
}

export default Sidebar
