import styled from 'styled-components'


export const PsecContainer = styled.div`
 background: ${({bg})=> bg ? '#CD5C5C' : '#008080'} ;
`
export const PsecWrapper = styled.div`
 width:100%;
 height:860px;
 padding: 0px 16px;
 display:grid;
 grid-template-rows:1fr 1fr 1fr;
 justify-content:center;
 align-items:center;
`
export const PsecItems = styled.div`
 justify-content:center;
 align-items:center;
 display:flex;
 padding: 15px 15px;
 width:100%;
 height:100%;
 
`
export const PsecContent = styled.div`

 font-size: ${({size})=> size ? '100px' : '80px'} ;
 color: ${({color})=> color ? '#CD5C5C' : '#008080'};
 cursor: pointer;
 margin:16px 6px;
 
`
export const IconStore = styled.div`
 position:relative;
 align-items:center;
 margin:16px 6px;
 width:100%;
 height:100%;
 display:block;
`
