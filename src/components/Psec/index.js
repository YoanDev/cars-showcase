import React from 'react'
import { IoArrowRedoCircle as Iarrow, IoBanSharp as Isharp, IoCarSport as Icar } from "react-icons/io5";
import { PsecContainer, PsecWrapper, PsecItems, PsecContent, IconStore } from './psecElements';

const Psec = ({bg, size, color}) => {
    return (
        <>
            <PsecContainer bg={bg}>
                <PsecWrapper>
                    <PsecItems>
                        <PsecContent size={size} color={color}>
                            <IconStore>
                                <Iarrow />
                            </IconStore>
                            
                        </PsecContent>
                        <PsecContent size={size} color={color}>
                            <IconStore>
                            <Isharp />
                            </IconStore>
                        </PsecContent>
                        <PsecContent size={true} color={color}>
                            <IconStore>
                               <Icar /> 
                            </IconStore>
                        </PsecContent>

                    </PsecItems>
                </PsecWrapper>
            </PsecContainer>
        </>
    )
}

export default Psec
