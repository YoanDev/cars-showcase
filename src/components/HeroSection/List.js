import Aos from 'aos'
import 'aos/dist/aos.css'
import React, {useState, useEffect} from 'react'
import {  CarGrid, HeroBg, HeroContainer, HeroContent, HeroH1, HeroP, ImageBg, MarqueBanner, MarqueContainer } from './HeroElements'
import Nissan from '../../images/logo-Nissan.png'
import { Modal } from './Modal/modal'


const List = ({people}) => {
    const [ReadMore, SetReadMore] = useState(false)
    const [index, setIndex] = useState(0)
    const {id, image, text, theme} = people[index]
    const [slide, setSlide] = useState(false)
    const [showModal, setShowModal] = useState(false)


    
  const openModal = ()=>{
    setShowModal(prev => !prev)
  }

    const checkNumber = (number) =>{
        if(number > people.length -1){
            return 0
        }
        if(number < 0){
            return people.length - 1
        }
        return number
    }
    
    const nextPerson = () => {
        setIndex(()=>{
            let newIndex = index + 1
            return checkNumber(newIndex)
        })
    }

    const prevPerson = () => {
        setIndex(()=>{
            let newIndex = index - 1
            return checkNumber(newIndex)
        })
    }

    function Slider(slide){
        // traitement
        if(slide = true){
            var timer = setTimeout(nextPerson,3000); /* rappel après 3 secondes = 3000 millisecondes */
            
        }
        
        }

    function stop (){
        clearInterval(Slider)
        setSlide(!slide)
    }
         

    useEffect(()=>{
        Slider(slide)
    }, [slide])
    
    useEffect(()=>{
        Aos.init({duration:2000})
    }, [])

    

    

    
            
                return (
                    
                    <>
                    <HeroContainer onClick={openModal}  data-aos='fade-zoom-in'  id={id}>
                        <HeroBg
                         
                        onMouseOver={Slider(slide)}
                        id={theme} >

                            <ImageBg  src={image} />
                        </HeroBg>
                        <HeroContent>
                            <HeroH1 data-aos='fade-zoom-in'>{} </HeroH1>
                            <HeroP></HeroP>
                            <MarqueContainer>
                                <MarqueBanner src={Nissan} />

                            </MarqueContainer>
                        
                        
                            
                        </HeroContent>
                        
                </HeroContainer>
                <Modal showModal={showModal} setShowModal={setShowModal}/>
                </>
                )

            
        
    
}

export default List
