import React, { useRef, useEffect, useCallback } from 'react';
import { useSpring, animated } from 'react-spring';
import NissanModal from '../../../images/nissan-370z1.jpg'
import LogoNissan from '../../../images/logo-Nissan.png'
import { Background, CloseModalButton, ModalButton, ModalContent, ModalImg, ModalP, ModalWrapper, ModalLogo, ModalH1 } from './ModalElements';



export const Modal = ({ showModal, setShowModal }) => {
  const modalRef = useRef();

  const animation = useSpring({
    config: {
      duration: 250
    },
    opacity: showModal ? 1 : 0,
    transform: showModal ? `translateY(0%)` : `translateY(-100%)`
  });

  const closeModal = e => {
    if (modalRef.current === e.target) {
      setShowModal(false);
    }
  };

  const keyPress = useCallback(
    e => {
      if (e.key === 'Escape' && showModal) {
        setShowModal(false);
        console.log('I pressed');
      }
    },
    [setShowModal, showModal]
  );

  useEffect(
    () => {
      document.addEventListener('keydown', keyPress);
      return () => document.removeEventListener('keydown', keyPress);
    },
    [keyPress]
  );

  return (
    <>
      {showModal ? (
        <Background onClick={closeModal} ref={modalRef}>
          <animated.div style={animation}>
            <ModalWrapper showModal={showModal}>
              <ModalImg src={NissanModal} alt='nissan' />
              <ModalContent>
                  <ModalLogo src={LogoNissan} />
                <ModalH1>Welcome to Nissan</ModalH1>
                <ModalP>Get exclusive access to our next launch.</ModalP>
                <ModalButton>Go to Nissan official website</ModalButton>
              </ModalContent>
              <CloseModalButton
                aria-label='Close modal'
                onClick={() => setShowModal(prev => !prev)}
              />
            </ModalWrapper>
          </animated.div>
        </Background>
      ) : null}
    </>
  );
};