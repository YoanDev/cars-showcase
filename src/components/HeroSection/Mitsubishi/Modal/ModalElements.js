import styled from 'styled-components';
import { MdClose } from 'react-icons/md';


export const Background = styled.div`
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.8);
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 200;
  top:0;
  left:0;
`;

export const ModalWrapper = styled.div`
  width: 80%;
  height: 70vh;
  box-shadow: 0 5px 16px rgba(0, 0, 0, 0.2);
  background: #fff;
  color: #000;
  display: grid;
  grid-template-columns: 1fr 1fr;
  position: relative;
  z-index: 200;
  border-radius: 10px;
  top:0;
  left:10%;
  box-sizing:border-box;

  @media screen and (max-width:768px){
      left:0;
      width:100%;
      height:45vh;
  }
`;

export const ModalImg = styled.img`
  width: 100%;
  height: 100%;
  border-radius: 10px 0 0 10px;
  background: #000;
  z-index: 200;

`;

export const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  line-height: 1.8;
  color: #141414;
  z-index: 200;

`


export const ModalLogo = styled.img`
 width:30%;
 height:20%;
`

export const ModalH1 = styled.h1`
 margin-bottom:1rem;

 @media screen and (max-width:768px){
      margin-bottom:0rem;
      font-size:26px;
  }
`

export const ModalP = styled.p`
  
    margin-bottom: 1rem;

    @media screen and (max-width:768px){
      margin-bottom:0.5rem;
  }
  
` 
   

export const ModalButton = styled.button `
    padding: 10px 24px;
    background: #141414;
    color: #fff;
    border: none;
    cursor: pointer;
  `
   



export const CloseModalButton = styled(MdClose)`
  cursor: pointer;
  position: absolute;
  top: 20px;
  right: 20px;
  width: 32px;
  height: 32px;
  padding: 0;
  z-index: 3000;
`;