import React, { useRef, useEffect, useCallback } from 'react';
import { useSpring, animated } from 'react-spring';
import MitsubishiModal from '../../../../images/mitsubishi-lanevo6.jpg'
import LogoMitsubishi from '../../../../images/logo-Mitsubishi.png'
import { Background, CloseModalButton, ModalButton, ModalContent, ModalImg, ModalP, ModalWrapper, ModalLogo, ModalH1 } from './ModalElements';



export const Modal = ({ showModal2, setShowModal2 }) => {
  const modalRef2 = useRef();

  const animation2 = useSpring({
    config: {
      duration: 250
    },
    opacity: showModal2 ? 1 : 0,
    transform: showModal2 ? `translateY(0%)` : `translateY(-100%)`
  });

  const closeModal2 = e => {
    if (modalRef2.current === e.target) {
      setShowModal2(false);
    }
  };

  const keyPress2 = useCallback(
    e => {
      if (e.key === 'Escape' && showModal2) {
        setShowModal2(false);
        console.log('I pressed');
      }
    },
    [setShowModal2, showModal2]
  );

  useEffect(
    () => {
      document.addEventListener('keydown', keyPress2);
      return () => document.removeEventListener('keydown', keyPress2);
    },
    [keyPress2]
  );

  return (
    <>
      {showModal2 ? (
        <Background onClick={closeModal2} ref={modalRef2}>
          <animated.div style={animation2}>
            <ModalWrapper showModal={showModal2}>
              <ModalImg src={MitsubishiModal} alt='mitsubishi' />
              <ModalContent>
                  <ModalLogo src={LogoMitsubishi} />
                <ModalH1>Welcome to Mitsubishi</ModalH1>
                <ModalP>Get exclusive access to our next launch.</ModalP>
                <ModalButton>Go to Mitsubishi official website</ModalButton>
              </ModalContent>
              <CloseModalButton
                aria-label='Close modal'
                onClick={() => setShowModal2(prev => !prev)}
              />
            </ModalWrapper>
          </animated.div>
        </Background>
      ) : null}
    </>
  );
};