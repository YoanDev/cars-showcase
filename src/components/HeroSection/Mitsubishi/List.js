import Aos from 'aos'
import 'aos/dist/aos.css'
import React, {useState, useEffect} from 'react'
import {  CarGrid, HeroBg, HeroContainer, HeroContent, HeroH1, HeroP, ImageBg, MarqueBanner, MarqueContainer } from '../../HeroSection/HeroElements'
import Mitsubishi from '../../../images/logo-Mitsubishi.png'
import {Modal} from '../Mitsubishi/Modal/modal'
import './style.css'

const ListMitsubishi = ({people2}) => {
    const [ReadMore, SetReadMore] = useState(false)
    const [index, setIndex] = useState(0)
    const {id, image, text, theme} = people2[index]
    const [slide, setSlide] = useState(false)
    const [showModal2, setShowModal2] = useState(false)


    
  const openModal2 = ()=>{
    setShowModal2(prev => !prev)
  }

    const checkNumber = (number) =>{
        if(number > people2.length -1){
            return 0
        }
        if(number < 0){
            return people2.length - 1
        }
        return number
    }
    
    const nextPerson = () => {
        setIndex(()=>{
            let newIndex = index + 1
            return checkNumber(newIndex)
        })
    }

    const prevPerson = () => {
        setIndex(()=>{
            let newIndex = index - 1
            return checkNumber(newIndex)
        })
    }

    function Slider(slide){
        // traitement
        if(slide = true){
            setTimeout(nextPerson,3000); /* rappel après 3 secondes = 3000 millisecondes */
            
        }
        
        }

    function stop (){
        clearInterval(Slider)
        setSlide(!slide)
    }
         

    useEffect(()=>{
        Slider(slide)
    }, [slide])
    
    useEffect(()=>{
        Aos.init({duration:2000})
    }, [])

    

    

    
            
                return (
                    
                    <>
                    <HeroContainer onClick={openModal2}  data-aos='fade-zoom-in'  id={id}>
                        <HeroBg
                        onMouseEnter={()=> setSlide(slide = true)}
                        onMouseLeave={()=> stop()}
                        onMouseOver={Slider(slide)}
                        id={theme} >

                            <ImageBg  src={image} />
                        </HeroBg>
                        <HeroContent>
                            <HeroH1 data-aos='fade-zoom-in'>{} </HeroH1>
                            <HeroP></HeroP>
                            <MarqueContainer>
                                <MarqueBanner src={Mitsubishi} id='logo-mitsubishi' />

                            </MarqueContainer>
                        
                            
                        </HeroContent>
                </HeroContainer>
                <Modal showModal2={showModal2} setShowModal2={setShowModal2}/>
                
                </>
                )

            
        
    
}

export default ListMitsubishi
