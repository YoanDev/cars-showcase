import React, { useState } from 'react'

import List from './List'
import Data from './Data'
import Data2 from '../HeroSection/Mitsubishi/Data'
import { CarGrid } from './HeroElements'
import ListMitsubishi from './Mitsubishi/List'

const HeroSection = (id, image, text) => {
    const [hover, setHover] = useState(false)
    const [people, setPeople] = useState(Data)
    const [people2, setPeople2] = useState(Data2)


    const onHover = () => {
        setHover(!hover)
    }

    

    return (
        <CarGrid>
            <List  people={people} />
            <ListMitsubishi people2={people2} />
        </CarGrid>
        
    )
}

export default HeroSection
