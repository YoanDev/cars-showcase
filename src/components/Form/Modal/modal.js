import React, { useRef, useEffect, useCallback } from 'react';
import {useForm} from 'react-hook-form'
import { useSpring, animated } from 'react-spring';
import { ModalInput, ModalLabel, ModalSubmit, ModalSubmitText, CloseModalButton, Background, ModalForm, ModalContent } from './FormElements';



export const Modal = ({ showModal, setShowModal }) => {
  const modalRef = useRef();

  const animation = useSpring({
    config: {
      duration: 250
    },
    opacity: showModal ? 1 : 0,
    transform: showModal ? `translateY(0%)` : `translateY(-100%)`
  });

  const closeModal = e => {
    if (modalRef.current === e.target) {
      setShowModal(false);
    }
  };

  const keyPress = useCallback(
    e => {
      if (e.key === 'Escape' && showModal) {
        setShowModal(false);
        console.log('I pressed');
      }
    },
    [setShowModal, showModal]
  );

  useEffect(
    () => {
      document.addEventListener('keydown', keyPress);
      return () => document.removeEventListener('keydown', keyPress);
    },
    [keyPress]
  );


  const wait = function(duration = 1000){
    return new Promise((resolve) =>{
      window.setTimeout(resolve, duration)
    })
  }


  const {register, handleSubmit, formState, errors, SetError} = useForm({
    mode: 'onTouched'
  })


  const {isSubmitting, isSubmitted, isSubmitSuccessful} = formState


  const onSubmit = async data => {
    await wait(2000)

    SetError('username', {
      type:'manual',
      message:"Server don't understand values"
    })
  }


  console.log(isSubmitted, isSubmitSuccessful)

  return (
    <>
      {showModal ? (
        <Background onClick={closeModal} ref={modalRef}>
          <animated.div style={animation}>
            <ModalForm showModal={showModal} onSubmit={onSubmit} >
              {isSubmitSuccessful && console.log('request succeed')  }
              <ModalContent>
                <ModalLabel for='name' >Username</ModalLabel>
                <ModalInput defaultValue="JohnDoe" type='text' id='name' name='user_name' ref={register({required: 'you need to enter a username', minLength:{value:3, message:'need at least 3 characters'} })} />
                {errors.username && <span>{errors.username.message} </span>}
              </ModalContent>
              <ModalContent>
                <ModalLabel for='email' >Email</ModalLabel>
                <ModalInput defaultValue='John@Doe.fr' type='email' id='email' name='email' ref={register({required: true})} />
              </ModalContent>
              <ModalContent>
                <ModalLabel for='password' >Password</ModalLabel>
                <ModalInput  type='password' id='password' name='password' ref={register({required: true})} />
              </ModalContent>
              <ModalSubmit disabled={(isSubmitting)}>
                <ModalSubmitText>Submit</ModalSubmitText>
              </ModalSubmit>
              <CloseModalButton
                aria-label='Close modal'
                onClick={() => setShowModal(prev => !prev)}
              />
            </ModalForm>
          </animated.div>
        </Background>
      ) : null}
    </>
  );
};