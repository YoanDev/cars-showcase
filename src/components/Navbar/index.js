import Aos from 'aos'
import 'aos/dist/aos.css'
import React, {useEffect, useState, useRef} from 'react'
import { MobileIcon, Nav, NavbarContainer, NavBtn, NavBtnLink, NavItems, NavLinks, NavLogo, NavMenu } from './NavbarElements'
import {FaBars} from 'react-icons/fa'
import {animateScroll as scroll} from 'react-scroll'
import useDocumentScrollThrottled from './useDocumentScrollThrottled';
import {Modal} from '../Form/Modal/modal'

const Navbar = ({toggle}) => {

    const [shouldHideHeader, setShouldHideHeader] = useState(false);
    const [shouldShowShadow, setShouldShowShadow] = useState(false);

  
    const MINIMUM_SCROLL = 80;
    const TIMEOUT_DELAY = 400;
  
    useDocumentScrollThrottled(callbackData => {
      const { previousScrollTop, currentScrollTop } = callbackData;
      const isScrolledDown = previousScrollTop < currentScrollTop;
      const isMinimumScrolled = currentScrollTop > MINIMUM_SCROLL;
  
      setShouldShowShadow(currentScrollTop > 2);
  
      setTimeout(() => {
        setShouldHideHeader(isScrolledDown && isMinimumScrolled);
      }, TIMEOUT_DELAY);
    });
  
    const shadowStyle = shouldShowShadow ? 'shadow' : '';
    const hiddenStyle = shouldHideHeader ? 'hidden' : '';
  

    const [showModal, setShowModal] = useState(false)

    const openModal = ()=>{
        setShowModal(prev => !prev)
      }
    

    const toggleHome = ()=>{
        scroll.scrollToTop();
    }



    useEffect(()=>{
        Aos.init({duration:2000})
    }, [])
    
    return (
        <>
        <Nav
        className={`header ${shadowStyle} ${hiddenStyle}`}
         >
            <NavbarContainer data-aos='fade-left'>
                <NavLogo  to='/'  onClick={toggleHome} >Cars Showcase</NavLogo>
                <MobileIcon onClick={toggle}>
                    <FaBars/>
                </MobileIcon>
                <NavMenu>
                    <NavItems>
                        <NavLinks to='/concessionaire'>Concessionaire</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='/ecology' smooth={true} duration={500} spy={true} exact='true' offset={-80}>Ecology</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='/contact-us' smooth={true} duration={500} spy={true} exact='true' offset={-80}>Contact Us</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='/customer-service' smooth={true} duration={500} spy={true} exact='true' offset={-80}>Customer Service</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks onClick={openModal}  smooth={true} duration={500} spy={true} exact='true' offset={-80}>Register in</NavLinks>
                    </NavItems>
                    
                    
                    
                        
                </NavMenu>

               
            </NavbarContainer>
        </Nav>
        <Modal showModal={showModal} setShowModal={setShowModal}/>
        </>
    )
}

export default Navbar
