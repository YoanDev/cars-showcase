import React, {useState} from 'react'
import Navbar from '../components/Navbar'
import Psec from '../components/Psec'


const Playground = ({bg}) => {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = ()=>{
        setIsOpen(!isOpen)
    }

    return (
        <>
            <Navbar toggle={toggle} />
            <Psec bg={true} />
            <Psec color={true} />
        </>
    )
}

export default Playground
