import React from 'react'
import { Link } from 'react-router-dom'
import ErrorImage from '../images/on-work2.svg'
import {BsFillHouseDoorFill as HomeIcon, BsArrowDown as DownArrow} from 'react-icons/bs'

const Error = () => {
    return (
        <div>
            <h2 className='error-text'> 404 not found. We are working on this section. </h2>

            <div className='error-arrow'><DownArrow /> </div>

            <Link to='/'>
                <h3 className='error-home-redirect' ><HomeIcon /> </h3>
            </Link>
            


            <img className='error-page' src={ErrorImage} alt=""/>

        </div>
    
    )
}

export default Error
