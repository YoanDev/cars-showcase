import './App.css';
import {BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom'
import Home from './pages/index';
import Error from './pages/error';
import Playground from './pages/playground';

function App() {
  return (
    <>
    <Router>
      <Switch>
        <Route path='/' component={Home} exact />
        <Route path='/playground' component={Playground} exact />
        <Route path='*'>
          <Error></Error>
        </Route>
      </Switch>
    </Router>
    </>
  );
}

export default App;
